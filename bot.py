class Bot:

    def __init__(self):
        self.position = [1,1]
        self.size = [0,0]

    def move(self,direction):
        if direction == 'up':
            self.position[1] -= 1
        elif direction == 'down':
            self.position[1] += 1
        elif direction == 'left':
            self.position[0] -= 1
        elif direction == 'right':
            self.position[0] += 1

    def grow(self,direction):
        if direction == 'up':
            self.position[1] -= 1
            self.size[1] += 1
        elif direction == 'down':
            self.size[1] += 1
        elif direction == 'left':
            self.position[0] -= 1
            self.size[0] += 1
        elif direction == 'right':
            self.size[0] += 1

    def shrink(self,direction):
        if direction == 'up' or direction == 'down':
            if self.size[1] != 0:
                if direction == 'up':
                    self.position[1] += 1
                self.size[1] -= 1
        elif direction == 'left' or direction == 'right':
            if self.size[0] != 0:
                if direction == 'left':
                    self.position[0] += 1
                self.size[0] -= 1

    def check_direction(self,board,direction):
        legal_move = True
        board_cols = len(board)
        board_rows = len(board[0])
        
        if direction == 'up':
            if self.position[1] == 0:
                legal_move = False
            else:
                for x in range(self.position[0], self.position[0] + self.size[0] + 1):
                    if int(board[x][self.position[1] - 1]) == 2:
                        legal_move = False
        elif direction == 'down':
            if self.position[1] == board_rows - 1:
                legal_move = False
            else:
                for x in range(self.position[0], self.position[0] + self.size[0] + 1):
                    if int(board[x][self.position[1] + self.size[1] + 1]) == 2:
                        legal_move = False
        elif direction == 'left':
            if self.position[0] == 0:
                legal_move = False
            else:
                for y in range(self.position[1], self.position[1] + self.size[1] + 1):
                    if int(board[self.position[0] - 1][y]) == 2:
                        legal_move = False
        elif direction == 'right':
            if self.position[0] == board_cols - 1:
                legal_move = False
            else:
                for y in range(self.position[1], self.position[1] + self.size[1] + 1):
                    if int(board[self.position[0] + self.size[0] + 1][y]) == 2:
                        legal_move = False

        return legal_move