def read_boards(filename):
    """Creates and returns an array containing boards imported from file."""
    
    file_boards = open(filename, 'r')
    lines = file_boards.readlines()
    file_boards.close()
    
    num_boards = int(lines[0])
    boards = [0 for i in xrange(num_boards)]
    line_counter = 1
    
    for i in range(0, num_boards):
        dimensions = lines[line_counter].split()
        x = int(dimensions[0])
        y = int(dimensions[1])
        boards[i] = [[0 for dim_y in range(y)] for dim_x in range(x)]
        line_counter += 1
        
        for j in range(line_counter, line_counter + y):
            board_line = lines[j]
            for k in range(0, x):
                if board_line[k] is '2':
                    boards[i][k][j-line_counter] = 2
        line_counter += y
    
    return boards