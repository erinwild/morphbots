import sys
import pygame
from bot import Bot
from boards import read_boards

boards = read_boards(sys.argv[1])

# Initialize the game engine
pygame.init()

# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
BLUE     = (   0,   0, 255)
GREEN    = (   0, 255,   0)
YELLOW   = ( 255, 255,   0)

board_num = 1
numcols = len(boards[board_num])
numrows = len(boards[board_num][0])

# Set the height and width of the screen
width = 30
height = 30
margin = 3
bot_margin = 8
size = (numcols * width + (numcols + 1) * margin, numrows * height + (numrows + 1) * margin)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Morphbots")

done = False

morphbot = Bot()

while not done:
    # --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if morphbot.check_direction(boards[board_num],'up'):
                    morphbot.move('up')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Moved up"
            elif event.key == pygame.K_DOWN:
                if morphbot.check_direction(boards[board_num],'down'):
                    morphbot.move('down')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Moved down"
            elif event.key == pygame.K_LEFT:
                if morphbot.check_direction(boards[board_num],'left'):
                    morphbot.move('left')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Moved left"
            elif event.key == pygame.K_RIGHT:
                if morphbot.check_direction(boards[board_num],'right'):
                    morphbot.move('right')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Moved right"
            elif event.key == pygame.K_w:
                if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                    morphbot.shrink('up')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Shrunk from top"
                else:
                    if morphbot.check_direction(boards[board_num],'up'):
                        morphbot.grow('up')
                        for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                            for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                                boards[board_num][i][j] = 1
                        print "Grew up"
            elif event.key == pygame.K_s:
                if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                    morphbot.shrink('down')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Shrunk from bottom"
                else:
                    if morphbot.check_direction(boards[board_num],'down'):
                        morphbot.grow('down')
                        for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                            for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                                boards[board_num][i][j] = 1
                        print "Grew down"
            elif event.key == pygame.K_a:
                if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                    morphbot.shrink('left')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Shrunk from left"
                else:
                    if morphbot.check_direction(boards[board_num],'left'):
                        morphbot.grow('left')
                        for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                            for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                                boards[board_num][i][j] = 1
                        print "Grew left"
            elif event.key == pygame.K_d:
                if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                    morphbot.shrink('right')
                    for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                        for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                            boards[board_num][i][j] = 1
                    print "Shrunk from right"
                else:
                    if morphbot.check_direction(boards[board_num],'right'):
                        morphbot.grow('right')
                        for i in range(morphbot.position[0], morphbot.position[0] + morphbot.size[0] + 1):
                            for j in range(morphbot.position[1], morphbot.position[1] + morphbot.size[1] + 1):
                                boards[board_num][i][j] = 1
                        print "Grew right"


    # Clear screen to black
    screen.fill(BLACK)

    # Draw board
    boards[board_num][1][1] = 1
    for column in range(numcols):
        for row in range(numrows):
            color = WHITE
            if boards[board_num][column][row] == 2:
                color = BLUE
            elif boards[board_num][column][row] == 1:
                color = YELLOW
            pygame.draw.rect(screen,color,[margin+(margin+width)*column,margin+(margin+height)*row,width,height])

    # Draw morphbot
    pygame.draw.rect(screen,GREEN,[margin + morphbot.position[0]*(width+margin) + bot_margin, margin + morphbot.position[1]*(height+margin) + bot_margin, width + morphbot.size[0]*(width+margin) - 2*bot_margin,height + morphbot.size[1]*(height+margin) - 2*bot_margin])


    # Update screen
    pygame.display.flip()

pygame.quit()